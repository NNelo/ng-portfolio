# Nelo Nanfara Portfolio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

With the help of [Paper Kit 2](https://www.creative-tim.com/product/paper-kit-2-angular) 


## Web Page

Visit my portfolio [here](https://nelonanfara.herokuapp.com/#/)

## Email server

Find the source code [here](https://gitlab.com/NNelo/ng-portfolio-mailing)
