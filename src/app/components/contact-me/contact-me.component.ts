import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-contact-me',
  templateUrl: './contact-me.component.html',
  styleUrls: ['./contact-me.component.scss']
})
export class ContactMeComponent implements OnInit {

  msgForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    msg: new FormControl('', Validators.required)
  })

  public sent: boolean;

  focus: any;
  focus1: any;

  constructor(private http: HttpClient) {
    this.sent = false;
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.sendEmail();
    this.msgForm.patchValue({
      name: '',
      email: '',
      msg: ''
    });
    this.sent = true;
  }

  sendEmail() {
    if (this.msgForm.valid) {
      const form = this.msgForm.value;
      const headers = new HttpHeaders({'Content-Type': 'application/json'});
      this.http.post('https://portfolio-mailing.herokuapp.com' + '/sendEmail',
        {name: form.name, emailadr: form.email, message: form.msg},
        {'headers': headers}
      ).subscribe();
    }
  }
}
