import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NouisliderModule} from 'ng2-nouislider';
import {JwBootstrapSwitchNg2Module} from 'jw-bootstrap-switch-ng2';
import {RouterModule} from '@angular/router';

import {ContactMeComponent} from './contact-me/contact-me.component';
import {AboutMeComponent} from './about-me/about-me.component';
import {ProfileComponent} from "./profile/profile.component";
import { ProjectsComponent } from './projects/projects.component';
import { PresentComponent } from './present/present.component';
import { MainCardComponent } from './main-card/main-card.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import { FooterComponent } from './footer/footer.component';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    NouisliderModule,
    RouterModule,
    JwBootstrapSwitchNg2Module,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatIconModule,
    MatButtonModule,
    TranslateModule
  ],
  declarations: [
    ContactMeComponent,
    AboutMeComponent,
    ProfileComponent,
    ProjectsComponent,
    PresentComponent,
    MainCardComponent,
    FooterComponent
  ],
  entryComponents: [],
  exports: [ContactMeComponent, AboutMeComponent]
})
export class ComponentsModule {
}
