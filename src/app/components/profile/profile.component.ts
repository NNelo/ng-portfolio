import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {MatIconRegistry} from "@angular/material/icon";
import {TranslateService} from "@ngx-translate/core";


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

    isDarkTheme : boolean;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public translate: TranslateService) {
    this.isDarkTheme = false;
    iconRegistry.addSvgIcon(
      'thumbs-up',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/examples/thumbup-icon.svg'));
      translate.addLangs(['es', 'en']);
      translate.setDefaultLang('es');
  }

    ngOnInit() {}

  switchLang(lang: string) {
    this.translate.use(lang);
  }

}
